# INTEGRATOR PROJECT I

## 3er 4 Month Term - Afternoon - UTHermosillo

---

# TEAM #1

#### Project: Name of proyect

---

### Welcome to your REPO in GitLab, so your work will be allocated, stored, and monitored here in on place.

#### You have to include the same data that is prepared for the base documentation:

- Description of the Problem to be Solved
 - El sistema ayudará a la empresa Hīrō anime shopping  a llegar a un público objetivo que compra por    internet, personas que no quieren salir de casa o esas personas que no tienen tiempo de poder acudir a la tienda debido a que está muy lejos o simplemente no pueden, también ayudará a la empresa a mantener y atraer nuevos clientes. 
- History of the problem
 - La empresa hiro vende productos de forma local y está teniendo una alta demanda de sus clientes locales y mensajes de redes sociales sobre las ventas por internet, el cliente no cuenta con este servicio; En consecuencia, los clientes locales se ven afectados por la falta de un servicio de internet igualmente que este negocio está perdiendo la capacidad de crecimiento y posibles nuevos clientes dado a la falta de una tienda virtual o ecommerce.

- Target market served by the solution
 - El mercado al que se dirige este sistema son todas aquellas personas que estén interesadas en comprar productos relacionados con anime tales como; ropa, calzado, figuras de anime, productos limitados, con ofertas.
- Identification of the problem (wording in question form)
 - ¿Como crear un sistema de aplicación web(ecommerce) que facilite la administración de la empresa, tenga un buen proceso de venta y que genere un crecimiento en clientes de 100 clientes  mínimos en un tiempo de menos de 2 meses después del deployment completo del sistema de la empresa Hīrō anime shopping?
- Solution Statement
 - Se necesita desarrollar una aplicación Web ecommerce para la empresa Hīrō anime shopping que permita a los clientes la compra de productos de anime merch principalmente; ropa, calzado, figuras de anime. Esta aplicación Web debe de estar diseñada para que cumpla con los requisitos de la empresa y que esta aplicación sea suficiente para atraer nuevos clientes en un tiempo mínimo determinado y tener la capacidad de crecer a largo plazo.

- General objective
 - Se desarrollará una aplicación Web Ecommerce de venta de productos de anime merchandising para el crecimiento del negocio de la empresa que lo usara.

- Project justification
 - La aplicación Hīrō anime shopping ecommerce es necesaria para tener un mayor alcance en los clientes de la empresa que le dará uso además de mejorar su proceso de venta y administrativo haciendo las cosas más fáciles y eficaces y con menos margen de error para la empresa.

## TEAM MEMBERS

### You list the team members here:

- Team member #1 https://gitlab.com/a20311083 a20311083@uthermosillo.edu.mx
- Team member #2

* Tema member #3
